package com.ztc.games.hitme.view;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.ztc.games.hitme.model.Block;
import com.ztc.games.hitme.model.Enemy;
import com.ztc.games.hitme.model.GameWorld;

/**
 * Created by Gillani on 1/16/2016.
 */
public class GameRenderer {

    private GameWorld world;
    private OrthographicCamera cam;

    private ShapeRenderer debugRenderer = new ShapeRenderer();

    /**
     *
     * @param world
     */
    public GameRenderer(GameWorld world) {
        this.world = world;
        this.cam = new OrthographicCamera(10, 7);
        this.cam.position.set(5, 3.5f, 0);
        this.cam.update();
    }

    public void render() {
        debugRenderer.setProjectionMatrix(cam.combined);
        debugRenderer.begin(ShapeRenderer.ShapeType.Line);
        for (Block block : world.getBlocks()) {
            Rectangle rect = block.getBounds();
            float x1 = block.getPosition().x + rect.x;
            float y1 = block.getPosition().y + rect.y;
            debugRenderer.setColor(new Color(1, 0, 0, 1));
            debugRenderer.rect(x1, y1, rect.width, rect.height);
        }

        Enemy enemy = world.getEnemy();
        Rectangle bounds = enemy.getBounds();

        float x1 = enemy.getPosition().x;
        float y1 = enemy.getPosition().y;

        debugRenderer.setColor(new Color(0, 1, 0, 1));
        debugRenderer.rect(x1, y1, bounds.width, bounds.height);

        debugRenderer.end();
    }
}

