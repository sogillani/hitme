package com.ztc.games.hitme.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Gillani on 1/16/2016.
 */
public class Enemy {

    static final float SIZE = 0.5f;

    Vector2 position;
    Rectangle bounds = new Rectangle();

    public Enemy(Vector2 position) {
        this.position = position;
        bounds.width = SIZE;
        bounds.height = SIZE;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setBounds(Rectangle bounds) {
        this.bounds = bounds;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }
}
