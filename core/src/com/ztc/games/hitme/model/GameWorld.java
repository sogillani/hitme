package com.ztc.games.hitme.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Gillani on 1/12/2016.
 */
public class GameWorld {

    private Array<Block> blocks = new Array<Block>();

    private Enemy enemy;

    /**
     *
     */
    public GameWorld() {
        createWorld();
    }


    /**
     *
     */
    public void createWorld() {

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 10; j++)
                blocks.add(new Block(new Vector2(j,i)));
        }

        enemy = new Enemy(new Vector2(5,3));
    }

    /**
     *
     * @param blocks
     */
    public void setBlocks(Array<Block> blocks) {
        this.blocks = blocks;
    }


    /**
     *
     * @return
     */
    public Array<Block> getBlocks() {
        return blocks;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }
}
