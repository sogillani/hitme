package com.ztc.games.hitme.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Gillani on 1/12/2016.
 */
public class Block {

    public static final float SIZE = 1f;

    private Vector2 position = new Vector2();
    private Rectangle bounds = new Rectangle();

    /**
     *
     * @param pos
     */
    public Block(Vector2 pos) {
        this.position = pos;
        this.bounds.width = SIZE;
        this.bounds.height = SIZE;
    }

    /**
     *
     * @return
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     *
     * @param position
     */
    public void setPosition(Vector2 position) {
        this.position = position;
    }

    /**
     *
     * @return
     */
    public Rectangle getBounds() {
        return bounds;
    }

    /**
     *
     * @param bounds
     */
    public void setBounds(Rectangle bounds) {
        this.bounds = bounds;
    }
}
