package com.ztc.games.hitme.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.ztc.games.hitme.model.GameWorld;
import com.ztc.games.hitme.view.GameRenderer;

/**
 * Created by Gillani on 1/16/2016.
 */
public class GameScreen implements Screen {

    private GameWorld world;
    private GameRenderer renderer;

    @Override
    public void show() {
        world = new GameWorld();
        renderer = new GameRenderer(world);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
